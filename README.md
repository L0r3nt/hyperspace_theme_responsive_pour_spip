# Hyperspace thème responsive pour SPIP

L'idée est de proposer un thème responsive pour [SPIP](http://www.spip.net/fr_rubrique91.html) __facile à installer__ sous forme de plugin.

Ce thème est une adaptation du thème [Hyperspace](https://html5up.net/hyperspace) proposé par le site [HTML5 UP](https://html5up.net/).

Le plugin s'adresse plutôt à un public averti (webmestre ou toute personne capable de mettre les mains dans le code), mais les évolutions ont pour objectif de faciliter sont installation et sa configuration pour un public moins technicien.

## Préconisation

__Le plugin est adapaté pour un site dont la structure est simple__, idéalement un site dont le contenu est une rubrique unique qui contient quelques articles. Le thème affiche un menu en partie gauche, et le contenu en parte droite. Tous les articles sont affichés les uns à la suite des autres, et le menu de gauche permet de faire défiler la zone de contenu jusqu'à l'article appellé.

Il a été mis en place au départ pour une petite association qui souhaitait avoir une présence sur le web pour présenter son activité.

Il est cependant possible de gérer des rubriques supplémentaires (actualités, partenaires, etc...), mais il faut garder à l'esprit que ce n'est pas sa fonction première.

## Installation

Le plugin s'installe comme tous les plugins SPIP.  
Copier le dossier dans plugins/ puis activer le plugin dans l’administration de SPIP.
Nécessite le plugin [Image Responsive](http://plugins.spip.net/image_responsive.html).

## Configuration

Le plugin propose une page de configuration dans le menu "Squelettes" de la partie privée de SPIP

Le plugin Hyperspace utilisant le plugin Image Responsive pour la gestion du responsive sur les images présentes dans le texte des articles, veuiller noter les indications données sur [la page de documentation d'Image Responsive](http://www.paris-beyrouth.org/tutoriaux-spip/article/plugin-spip-image-responsive) : 

> Si vous utilisez les URL « propres » dans SPIP, il faut modifier votre fichier .htaccess pour y ajouter le contenu du fichier ajouter_a_htaccess.txt livré avec le plugin.