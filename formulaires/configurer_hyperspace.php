<?php
/*
 * Plugin HYPERSPACE
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

function formulaires_configurer_hyperspace_charger_dist(){
	$valeurs = array(
		'hyperspace_rubrique_principale' => $GLOBALS['meta']['hyperspace_rubrique_principale'],
		'hyperspace_article_intro' => $GLOBALS['meta']['hyperspace_article_intro'],
		'hyperspace_rubrique_a' => $GLOBALS['meta']['hyperspace_rubrique_a'],
		'hyperspace_rubrique_b' => $GLOBALS['meta']['hyperspace_rubrique_b'],
		'hyperspace_rubrique_c' => $GLOBALS['meta']['hyperspace_rubrique_c'],

	);

	return $valeurs;
}

function formulaires_configurer_hyperspace_verifier_dist(){
	$erreurs = array();
 	// verifier que les champs obligatoires sont bien la :
	foreach(array('hyperspace_rubrique_principale','hyperspace_article_intro') as $obligatoire)
		if (!_request($obligatoire)) $erreurs[$obligatoire] = 'Ce champ est obligatoire';
 
    if (count($erreurs))
    	$erreurs['message_erreur'] = 'Votre saisie contient des erreurs !';
	return $erreurs;
}

function formulaires_configurer_hyperspace_traiter_dist(){
	
	include_spip('inc/meta');

	$hyperspace_rubrique_principale = _request('hyperspace_rubrique_principale');
	ecrire_meta('hyperspace_rubrique_principale', $hyperspace_rubrique_principale);
	
	$hyperspace_article_intro = _request('hyperspace_article_intro');
	ecrire_meta('hyperspace_article_intro', $hyperspace_article_intro);
	
	$hyperspace_rubrique_a = _request('hyperspace_rubrique_a');
	ecrire_meta('hyperspace_rubrique_a', $hyperspace_rubrique_a);

	$hyperspace_rubrique_b = _request('hyperspace_rubrique_b');
	ecrire_meta('hyperspace_rubrique_b', $hyperspace_rubrique_b);

	$hyperspace_rubrique_c = _request('hyperspace_rubrique_c');
	ecrire_meta('hyperspace_rubrique_c', $hyperspace_rubrique_c);


	// message
	return array(
		"editable" => true,
		"message_ok" => "Enregistrement validé.",
	);
}

?>
